# JNJ 마스터링 검수 프로세스 설명서

1. 대상 데이터 가져오기 (타겟/비타겟 분류)

2. 증정품 여부 골라내기 (tricky : 서비스의 도메인 지식 필요)

3. 엑셀 아웃풋 처리 및 master correct 체크
4. 3의 결과 기반으로 master 정보 수정
5. 타겟 데이터 골라내는 2번째 작업 (용량까지 고려)
6. colt_item_option 업로드



## 1. 대상 데이터 가져오기(`get_target`)

- master_id로 타겟/비타겟 분류
  - 지정된 경로에 타겟 master_id list 존재
  - 새로 추가된 마스터링 완료 데이터 가져온다 (dm1 = 타겟, dm2 = 비타겟)



## 2. 증정품 여부 골라내기(`finding_gift`)

- gift_classifier로 증정품 여부를 골라내는 작업 실행 (파일명: jnj_gift_classify_dict.xlsx)
- 결과:  dm1_hmm = 증정품 여부 결과의 육안 확인 필요 / dm1_final = 증정품 여부 확실히 체크된 리스트
- dm1_hmm은 육안으로 봐서 발라내고, dm1_final은 엑셀로 아웃풋 뽑아 다시 육안 확인



## 3. 엑셀 아웃풋 -> correct 체크

- 엑셀 아웃풋 실행시 필요한 fields : ['_id', 'goods_name', 'option_name', 'bundle_type', 'collect_url', 'master_dt', 'gift_yn', 'master_info_raw']

  - goods_name + option_name (+필요시 collect_url) 확인하면서 master_info_raw, bundle_type, gift_yn 표기가 정확한지 확인
  - 잘못된 정보 확인시 :
    - 엑셀에 wrong_type, before, after 컬럼을 만든다.
    - wrong_type 종류 : measure, unit, bundle_type, gift_yn, (+drop : 타겟아이템에서 제외)
    - before : 현재 기입된 value
    - after : 실제로 기입되어야 하는 value

- 필요한 도메인 지식

  - bundle_type(중요!)

    - S :  하나의 제품을 1개만 판매, 

      예시) 리스테린 쿨민트 750ml

    - SP : 하나의 제품을 여러개 판매,

      예시) 리스테린 쿨민트 750ml 2개

    - DP : 하나의 제품을 여러 용량으로 판매,  

      예시1) 리스테린 쿨민트 750ml+250ml 

      예시2) 그린티 마일드 250mlx8+(증정)랜덤1개

    - SC : 동일 카테고리이나 다른 제품 판매, 

      예시1) 리스테린 쿨민트 750ml + 그린티 마일드 750ml 

      예시2) 리스테린 쿨민트 750ml + (증)그린티마일드100ml

    - DC : 다른 카테고리 제품 판매,

      예시) 존슨즈베이비 핑크로션 500ml + 베이비오일 200ml

  - gift_yn

    - 증정품 O :  예시) 리스테린 쿨민트250ml + 가글컵

    - 증정품 X : 

      예시1) 리스테린 쿨민트250ml + (증)그린티마일드100ml  (같은 카테고리의 상품 증정) <-SC

      예시2) 뉴트로지나 핸드크림 30ml + (증)리스테린 그린티마일드 100ml <- DC 



## 4. master 정보 수정

- 육안 확인 완료된 엑셀을 불러와 이를 기반으로 수정사항들을 처리
- drop 해야할건 dm2에 append / 그외는 dm1_final로 계속 진행



## 5. 타겟 데이터 골라내기 2번째 작업(`check_target_ornot`)

- 이 단계에서는 master_id + "measure" 까지 고려하여 다시 타겟 아이템의 여부를 체크한다.
- "measure" 의 value가 타겟 리스트에 해당되지 않으면 drop (=> master_status = -1)
- result : dm1_final 과 dm1_drop으로 나옴
- dm1_drop 은 dm2에 append  / dm1_final은 계속 진행





## 6. colt_item_option에 master 정보 업로드

- dm2 

  => master_status = -1 + master_dt  업데이트

- dm1_final

  =>  master_status = 1

  =>  주피터에 기입된 코드 그대로 실행

- master_status 란?

  report 대상 데이터의 여부를 구분해놓는 field

  서비스팀에서 report 생성을 위해 사용하는 데이터는 master_status = 1 인 데이터 뿐임!