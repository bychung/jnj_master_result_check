## JNJ gift type check & update to colt_item_option
from datetime import datetime,timedelta
import pandas as pd
import numpy as np
import gc
import os
from itertools import chain
def subdoc_to_df(subdoc):
    return pd.DataFrame(list(chain.from_iterable(subdoc.to_list())))

import re
def gift_classifier(row, col, classift_list):
    gift_words = classift_list.loc[classift_list['구분'] == col]
    gift_word_list = gift_words['gift_words'].unique().tolist()
    for pattern in gift_word_list:
        match = re.search(pattern, row[col])
        if match:
            #print(pattern)
            #print(gift_words.loc[gift_words['gift_words'] == pattern, 'result'])
            return gift_words.loc[gift_words['gift_words'] == pattern, 'result'].values[0]
        else:
            continue
    return '없음'

import pymongo
host = '133.186.168.8'
port = 50000
client = pymongo.MongoClient(host,port)
mongodb = client['EPOP_JNJ']


## step2. colt_item_option_dm에서 타겟대상 뽑아온다. (master_dt지정)
def get_target(target_list_path, today, days_after = 1):
    target_list = pd.read_excel(target_list_path, index=False)
    target_list['target_master_id'] = target_list['target_id'].apply(lambda x: '_'.join(x.split('_')[:2]))
    target_id = target_list['target_master_id'].unique()
    in_list = {'$in':list(target_id)}
    out_list = {'$nin':list(target_id)}
    print('end day: ', today+timedelta(days=days_after))
    q = {'master_dt':{'$gte':today, '$lt':today+timedelta(days=days_after)}, 'master_info_raw.master_id':in_list}
    p = {'goods_name':1, 'option_name':1,'collect_url':1, 'bundle_type':1, 'master_info_raw.master_id':1, 'master_info_raw.measure':1, 'master_info_raw.count':1, 'master_info_raw.unit':1, 'master_dt':1}
    ## 타겟대상 아닌 것들 불러오기 (master_status update를 위해)
    q2 = {'master_dt':{'$gte':today, '$lt':today+timedelta(days=days_after)}, 'master_info_raw.master_id':out_list}
    dm2 = pd.DataFrame(list(mongodb['colt_item_option_dm'].find(q2, p)))
    if dm2.empty:
        print('>>non target list empty today!')
    else:
        exist_id_list = mongodb['colt_item_option'].find({'_id':{'$in':list(dm2._id.unique())}, 'master_status':{'$in':list([1,-1])}, 'master_info.master_id':{'$exists':True}}).distinct('_id')
        dm2 = dm2[~(dm2['_id'].isin(list(exist_id_list)))]
    # 타겟인 제품 가져온다.
    dm1 = pd.DataFrame(list(mongodb['colt_item_option_dm'].find(q, p)))
    if dm1.empty:
        print('>>target item list empty today!')
        return dm1, dm2
    # master된 데이터 중, 이미 master_info를 가진 것들은 업데이트 대상에서 제외한다.
    dm1_id_list = dm1['_id'].unique()
    exist_id_list = mongodb['colt_item_option'].find({'_id':{'$in':list(dm1_id_list)}, 'master_status':{'$in':list([1,-1])}, 'master_info.master_id':{'$exists':True}}).distinct('_id')
    dm1 = dm1[~(dm1['_id'].isin(list(exist_id_list)))]

    # colt_item_option 업데이트 용으로 데이터 다듬기
    dm1['master_info_raw'] = dm1['master_info_raw'].apply(lambda x:[{ k.replace('count', 'unit'): v for k, v in a.items() } for a in x])
    dm1['bundle_type'] = dm1['bundle_type'].apply(lambda x: x.upper())
    dm1['bundle_yn'] = dm1['bundle_type'].apply(lambda x: int(0) if x=='S' else int(1))
    dm1['bundle_yn'] = dm1['bundle_yn'].astype(int)
    return dm1, dm2

## step3. gift여부 파악하기
# gift dictionary가져오기
def finding_gift(gift_file_path, dm1):
    if dm1.empty:
        print('update list empty today!')
    gift_dict = pd.read_excel(gift_file_path)
    dm1['goods_name_gift'] = dm1.apply(lambda x: gift_classifier(x, 'goods_name', gift_dict), axis=1)
    dm1['option_name_gift'] = dm1.apply(lambda x: gift_classifier(x, 'option_name', gift_dict), axis=1)
    dm1 = dm1.mask(lambda x: x=='follow option_name')
    dm1 = dm1.mask(lambda x: x=='없음')
    dm1['gift_classify_result'] = dm1['option_name_gift'].fillna(dm1['goods_name_gift'])
    print(dm1['gift_classify_result'].unique())
    dm1.loc[dm1['gift_classify_result'] != 'gift_y', 'gift_yn'] = int(0)
    dm1.loc[dm1['gift_classify_result'] == 'gift_y', 'gift_yn'] = int(1)
    # 업데이트 보류할 부분 나누기
    # 보류part
    df1_hmm = dm1[(dm1['gift_classify_result'].isin(list(['DP', 'need_to_assign_bundle_type'])))&(dm1['gift_classify_result']!=dm1['bundle_type'])]
    # 업데이트 예정part
    df1_final = dm1[~((dm1['gift_classify_result'].isin(list(['DP', 'need_to_assign_bundle_type'])))&(dm1['gift_classify_result']!=dm1['bundle_type']))]

    return df1_hmm, df1_final

# measure랑 unit 고친 후에 이 과정을 거쳐서 타겟 아이템을 거른다.
def check_target_ornot(dm1):
    master_item = pd.DataFrame(list(client['EPOP_JNJ']['master_item'].find({},{'master_id':1,'sku_info.measure':1,'sku_info.srp':1})))
    master_info = master_item.groupby('_id')['sku_info'].apply(subdoc_to_df).reset_index()
    master_info = master_info[(master_info['measure']!='unknown')&(master_info['srp']!='unknown')]
    master_info['measure'] = master_info['measure'].astype(str)
    master_info['srp'] = master_info['srp'].astype(str)
    # step1. master_id와 measure를 합친 column을 만든다.
    master_info = master_info.assign(full_info = master_info['_id'] + '_' + master_info['measure'])
    dm1_info = dm1.groupby('_id')['master_info_raw'].apply(subdoc_to_df).reset_index()
    dm1_info = dm1_info.assign(full_info = dm1_info['master_id'] + '_' + dm1_info['measure'])

    # step2. full_info 로 master_status = -1 과 1 을 구분한다.
    drop_list = dm1_info.loc[~(dm1_info['full_info'].isin(list(master_info.full_info.unique().tolist()))), '_id'].unique().tolist()
    dm1_drop = dm1[dm1._id.isin(drop_list)].drop(columns = 'bundle_yn')
    dm1_final = dm1[~(dm1._id.isin(drop_list))]

    return dm1_final, dm1_drop


def execute_update(today, target_list_path, gift_file_path,save_file_path):
    dm1, dm2 = get_target(target_list_path, today)
    print('>>getting data done')
    if dm2.empty:
        print('non target list empty today')
    else:
        dm2_id_list = dm2['_id'].unique()
        ## 타겟아닌 것들은 master_status = -1로 업데이트해준다.
        mongodb['colt_item_option'].update_many({'_id':{'$in':list(dm2_id_list)}, 'master_status':{'$nin':list([-1, 1])}}, {'$set':{'master_status':int(-1)}})
        del dm2, dm2_id_list
        gc.collect()
    print('need to update date shape: ', dm1.shape)
    if dm1.shape[0] > 0:
        data_to_look, data_to_update = finding_gift(gift_file_path, dm1)
        # 업데이트 치기
        data_to_update['master_status'] = int(1)
        for _id, bundle_yn, bundle_type, master_info, master_status, gift_yn, master_dt in zip(data_to_update['_id'], data_to_update['bundle_yn'], data_to_update['bundle_type'], data_to_update['master_info_raw'], data_to_update['master_status'], data_to_update['gift_yn'], data_to_update['master_dt']):
            check = pd.DataFrame(list(mongodb['colt_item_option'].find({'_id':_id, 'master_status':{'$in':list([1,-1])}, 'master_info.master_id':{'$exists':True}}, {'master_status':1})))
            if check.empty:
                mongodb['colt_item_option'].update_one({'_id':_id}, {'$set':{'bundle_yn':bundle_yn, 'bundle_type':bundle_type, 'master_info':master_info, 'master_status':master_status, 'gift_yn':gift_yn, 'master_dt':master_dt}})
            else:
                print(f'{_id} is already_done')
                continue
        print('>>check gift and update colt_item_option done')
        # 업데이트한 데이터 저장 및 눈으로 봐야하는 데이터 저장
        today_date = today.strftime('%Y%m%d')
        print(today_date)
        data_to_look.to_pickle(f'{save_file_path}/{today_date}_data_to_look.pkl')
        data_to_update.to_pickle(f'{save_file_path}/{today_date}_data_updated.pkl')
    else:
        print('None of need to update')

def gift_daily(days_after=1):
    #parameter setting
    # 날짜 지정.
    today = datetime.now().replace(hour=0, minute=0,second=0,microsecond=0)
    print(today)
    ## step1. 존슨즈 타겟대상의 master_id를 불러온다.
    target_list_path = '/data/cby/JNJ/src/base/jnj_target_list.xlsx'
    gift_file_path = '/data/cby/JNJ/src/base/jnj_gift_classify_dict.xlsx'
    save_file_path = '/data/cby/JNJ/save_upload_file'
    os.makedirs(save_file_path, exist_ok=True)
    execute_update(today, target_list_path, gift_file_path, save_file_path, days_after)

# if __name__ == '__main__':
#     gift_daily()
